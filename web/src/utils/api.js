import config from "./config";

const apiProducto = {
  agregar: async (p) => await post("/product/", p),
  listar: async (filter) => await get(`/product/`, filter ? filter : null),
  actualizar: async (p) => await put(`/product/${p._id}`, p),
  borrar: async (id) => await del(`/product/${id}`),
};

const apiCarrito = {
  checkout: async (id) => await post(`/shoppingcart/checkout/${id}`, {}),
  crear: async (data) => await post(`/shoppingcart`, data),
  agregar: async (user_id, products) =>
    await put(`/shoppingcart/user/${user_id}`, products),
  listar: async (id) => await get(`/shoppingcart/${id}`),
  listarPor: async (filter) => await get(`/shoppingcart/q`, filter),
  actualizar: async (id, data) => await put(`/shoppingcart/${id}`, data),
  eliminar: async (id) => await del(`/shoppingcart/${id}`),
};

const apiLogin = {
  prefix: async () => {
    const result = await fetch(`${HOST}/prefix`);
    const data = await result.json();
    return data;
  },
  login: async (credentials) => {
    try {
      const result = await fetch(`${HOST}/user/login`, {
        method: "POST",
        body: JSON.stringify(credentials),
        headers: { "Content-Type": "Application/Json" },
      });
      const data = await result.json();
      return data;
    } catch (err) {}
  },
  register: async (form) => {
    try {
      const result = await fetch(`${HOST}/user/register`, {
        method: "POST",
        body: JSON.stringify(form),
        headers: { "Content-Type": "Application/Json" },
      });
      const response = await result.json();
      return response;
    } catch (err) {
      console.log(form);
      console.log("error " + err);
    }
  },
  user: async () => {
    let data;
    try {
      const result = await fetch(`${HOST}/auth/user`, {
        credentials: "include",
      });
      data = await result.json();
      return data;
    } catch (err) {
      console.log(data);
      console.log("error" + err);
    }
  },
  whoami: async (token) => {
    let data;
    try {
      const result = await fetch(`${HOST}/user/whoami`, {
        headers: { authorization: `${token}` },
      });
      data = await result.json();
      return data.content;
    } catch (err) {
      console.log(data);
      console.log("error" + err);
    }
  },
};

const { HOST, PORT } = config;
const baseUrl = `${HOST}`;

const get = async (endpoint, filter) => {
  const token = sessionStorage.getItem("token");
  let params = "";
  if (filter) {
    params = new URLSearchParams(filter);
  }
  try {
    const result = await fetch(`${baseUrl}${endpoint}?` + params, {
      headers: { authorization: `${token}` },
    });
    const data = await result.json();
    return data;
  } catch (err) {
    return { error: `${endpoint}: no se ha podido efectuar la operación` };
  }
};
const put = async (endpoint, body) => {
  const token = sessionStorage.getItem("token");
  try {
    const data = await fetch(`${baseUrl}${endpoint}`, {
      method: "PATCH",
      mode: "cors",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "Application/Json",
        authorization: `${token}`,
      },
    });
    return data;
  } catch (err) {
    return {
      error: `${endpoint}: no se ha podido efectuar la operación`,
    };
  }
};
const post = async (endpoint, body) => {
  const token = sessionStorage.getItem("token");
  try {
    const data = await fetch(`${baseUrl}${endpoint}`, {
      method: "POST",
      mode: "cors",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "Application/Json",
        authorization: `${token}`,
      },
    });
    return await data.json();
  } catch (err) {
    return {
      error: `${endpoint}: no se ha podido efectuar la operación`,
    };
  }
};
const del = async (endpoint) => {
  const token = sessionStorage.getItem("token");
  try {
    const data = await fetch(`${baseUrl}${endpoint}`, {
      method: "DELETE",
      headers: { authorization: `${token}` },
    });
    return data;
  } catch (err) {
    return { error: `${endpoint}: no se ha podido efectuar la operación` };
  }
};

const toQueryString = (obj) => {
  let str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
};

export { apiProducto, apiCarrito, apiLogin };
