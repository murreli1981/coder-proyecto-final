import "./App.css";
import React, { useEffect, useState, useContext } from "react";
import Search from "./components/Search";
import Card from "./components/Card";
import Cart from "./components/Cart";
import Chat from "./components/Chat";
import InputProduct from "./components/InputProduct";
import { apiCarrito, apiProducto, apiLogin } from "./utils/api";
import config from "./utils/config";
import LoginForm from "./components/Auth/LoginForm";
import AuthContext from "./store/auth-context";
import CartContext from "./store/cart-context";

const App = () => {
  const authCtx = useContext(AuthContext);
  const cartCtx = useContext(CartContext);

  useEffect(async () => {
    const getUser = async () => {
      const token = sessionStorage.getItem("token");
      const data = await apiLogin.whoami(token);

      authCtx.onLogin(data, token);
    };
    if (sessionStorage.getItem("token")) {
      await getUser();
    }
  }, []);

  const handleLogout = () => {
    authCtx.onLogout();
  };

  const handleChat = () => {
    if (!chatOn) {
      setChatOn(true);
    }
  };

  const [filter, setFilter] = useState({});
  const [admin, setAdmin] = useState([false]);
  const [prods, setProds] = useState([]);
  const [carrito, setCarrito] = useState([]);
  const [flag, setFlag] = useState("load");
  const [prodToEdit, setProdToEdit] = useState(false);
  const [chatOn, setChatOn] = useState(false);

  const fetchProduct = async () => {
    setAdmin(authCtx.user.admin);
    switch (flag) {
      case "load": {
        const resultProds = await apiProducto.listar(filter);
        setProds(resultProds.hasOwnProperty("error") ? [] : resultProds);
        let resultSc;
        if (cartCtx.id) {
          console.log("tenés id!");
          console.log(cartCtx.id);
          console.log("tenés id!");
          resultSc = await apiCarrito.listar(cartCtx.id);
        } else {
          console.log("no tiene id");
          console.log(cartCtx.id);
          console.log("no tiene id");
          resultSc = await apiCarrito.listarPor({ user: authCtx.user["id"] });
          console.log("carrito cargado");
          console.log(resultSc);
          console.log("carrito cargado");
        }
        if (Object.keys(resultSc).length > 0) {
          console.log("en el fetchProduct!");
          console.log(resultSc);
          cartCtx.loadItems(resultSc.products);
          cartCtx.addId(resultSc._id);
          setCarrito(resultSc.products);
        } else {
          console.log("carrito vacío");
          console.log(resultSc);
          console.log("carrito vacío");
          cartCtx.loadItems([]);
          setCarrito([]);
        }

        break;
      }
      case "load-carrito": {
        let resultSc;
        console.log("a ver el carrito");
        if (cartCtx.id) {
          console.log("hay id de carrito");
          resultSc = await apiCarrito.listar(cartCtx.id);
          console.log("listado de productos");
          console.log(resultSc);
        } else {
          console.log("no hay id de carrito");
          resultSc = await apiCarrito.listarPor({ user: authCtx.user["id"] });
        }
        cartCtx.loadItems(resultSc.products || []);
        setCarrito(resultSc.products || []);
        cartCtx.addId(resultSc._id);
        break;
      }
      case "updated": {
        setFlag("load-carrito");
        break;
      }
      case "product-updated": {
        setProdToEdit(false);
        setFlag("load");
        break;
      }
      case "product-edited": {
        setProdToEdit(false);
        setFlag("load");
        break;
      }
      case "cancel": {
        setProdToEdit(false);
        setFlag("load");
        break;
      }
      case "checkout": {
        setCarrito([]);
        cartCtx.addId(null);
        setFlag("load");
      }
      default: {
        break;
      }
    }
  };

  useEffect(() => {
    authCtx.user && fetchProduct();
  }, [authCtx.user, flag]);

  return (
    <React.Fragment>
      {authCtx.isLoggedIn && (
        <button onClick={handleChat} id="fixedbutton">
          CHATEAR CON SOPORTE!
        </button>
      )}
      {authCtx.isLoggedIn && chatOn && <Chat cb={setChatOn}></Chat>}
      {!authCtx.isLoggedIn && <LoginForm />}
      {authCtx.isLoggedIn && (
        <div className="App">
          {admin ? (
            <div
              class="btn add"
              style={{ width: "120px" }}
              onClick={(e) => {
                document.getElementById("input-product-form").style.display =
                  "flex";
              }}
            >
              Agregar producto
            </div>
          ) : (
            ""
          )}
          <div>
            <button onClick={handleLogout}>Logout</button>
          </div>

          {prodToEdit ? (
            <InputProduct cb={setFlag} prodToEdit={prodToEdit} />
          ) : (
            <InputProduct cb={setFlag} prodToEdit={false} />
          )}
          {prodToEdit
            ? (document.getElementById("input-product-form").style.display =
                "flex")
            : ""}

          <div class="row">
            <div class="column">
              <h2>Productos</h2>
              <Search cb={setFlag} setFilter={setFilter} />
              {prods.map((prod) => (
                <div key={prod.id}>
                  <Card
                    content={prod}
                    cb={setFlag}
                    admin={admin}
                    edit={setProdToEdit}
                  />
                </div>
              ))}
            </div>
            <div class="column">
              <h2>Carrito</h2>
              <Cart items={carrito} cb={setFlag} />
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default App;
