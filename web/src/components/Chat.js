import React, { useContext, useEffect, useRef } from "react";
import AuthContext from "../store/auth-context";
import { apiProducto } from "../utils/api";
import socketIOClient from "socket.io-client";
import config from "../utils/config";
const serverHost = config.HOST;
const classes = require("./Chat.module.css");

const Chat = ({ cb }) => {
  const authCtx = useContext(AuthContext);

  const socket = socketIOClient(serverHost, { transports: ["websocket"] });
  socket.emit("joinRoom", {
    user: authCtx.user.name,
    room: authCtx.user.username,
  });
  let chatArea = document.querySelector("#chat-area");
  const msgRef = useRef();
  const areaRef = useRef();

  useEffect(() => {
    socket.on("message", (msg) => {
      /**
       * firstLoad: true,
            messages: messages,
       */
      if (msg.firstLoad) {
        const messagesFromDB = msg.messages.map(
          (msg) => `<p>[${msg.username}][${msg.datetime}] ${msg.message}`
        );
        areaRef.current.innerHTML = messagesFromDB;
        console.log(messagesFromDB);
      } else {
        const p = document.createElement("P");
        const textnode = document.createTextNode(msg);
        p.appendChild(textnode);
        areaRef.current.appendChild(p);
      }
    });
  }, areaRef);

  const handleCancel = () => {
    socket.disconnect();
    cb(false);
  };

  const handleSend = () => {
    socket.emit("message", {
      user: authCtx.user.name,
      message: msgRef.current.value,
    });
    msgRef.current.value = "";
  };

  const handleEnter = (e) => {
    if (e.key == "Enter") {
      handleSend();
    }
  };

  return (
    <div className={classes.chatWindow}>
      <h4>Hola {authCtx.user.name}!</h4>
      <div id={"chat-area"} ref={areaRef}></div>
      <input
        type={"text"}
        placeholder="Ingrese un mensaje, <<enter>> o enviar para mandarlo"
        className={"message"}
        ref={msgRef}
        onKeyPress={handleEnter}
      ></input>
      <button onClick={handleSend}>Enviar</button>
      <p></p>
      <button onClick={handleCancel}>Cancelar</button>
    </div>
  );
};

export default Chat;
