import { Router as expressRouter } from "express";
import { json } from "express";
import cors from "cors";

const router = expressRouter();
router.use(cors());
router.use(json());


export default router;
