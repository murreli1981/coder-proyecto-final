import { Router as router } from "express";
import { secured } from "../../middlewares/authentication.middleware";
import Controller from "../../types/Controller";
const routes = router();

const shoppingCartRouter = (controller: Controller | any) => {
  routes.post("/checkout/:id", secured, controller.checkout);
  routes.get("/q", secured, controller.getByFilter);
  routes.get("/:id", secured, controller.getById);
  routes.get("/", secured, controller.getAll);
  routes.post("/", secured, controller.create);
  routes.patch("/:id", secured, controller.update);
  routes.delete("/:id", secured, controller.delete);

  return routes;
};

export default shoppingCartRouter;
