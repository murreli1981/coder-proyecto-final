import { Router as router } from "express";
import { admin } from "../../middlewares/admin.middleware";
import { secured } from "../../middlewares/authentication.middleware";
import Controller from "../../types/Controller";
const routes = router();

const orderRouter = (controller: Controller) => {
  routes.get("/q", secured, controller.getByFilter);
  routes.get("/:id", secured, controller.getById);
  routes.get("/", secured, controller.getAll);
  routes.post("/", secured, controller.create);
  routes.patch("/:id", secured, admin, controller.update);
  routes.delete("/:id", secured, admin, controller.delete);

  return routes;
};

export default orderRouter;
