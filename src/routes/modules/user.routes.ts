import { Router as router } from "express";
import Controller from "../../types/Controller";
const routes = router();
import { secured } from "../../middlewares/authentication.middleware";
import { admin } from "../../middlewares/admin.middleware";

const userRouter = (controller: Controller | any) => {
  routes.get("/whoami", secured, controller.whoami);
  routes.post("/register", controller.create);
  routes.post("/login", controller.login);
  routes.get("/q", secured, controller.getByFilter);
  routes.get("/:id", secured, controller.getById);
  routes.get("/", secured, controller.getAll);
  routes.patch("/:id", secured, controller.update);
  routes.delete("/:id", secured, admin, controller.delete);

  return routes;
};

export default userRouter;
