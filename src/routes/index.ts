import router from "./router";
import userRouter from "./modules/user.routes";
import productRouter from "./modules/product.routes";
import shoppingCartRouter from "./modules/shopping-cart.routes";
import messageRouter from "./modules/message.routes";
import orderRouter from "./modules/order.routes";

import controllers from "../controllers";

router.use("/user", userRouter(controllers.userController));
router.use("/product", productRouter(controllers.productController));
router.use(
  "/shoppingcart",
  shoppingCartRouter(controllers.shoppingCartController)
);
router.use("/message", messageRouter(controllers.messageController));
router.use("/order", orderRouter(controllers.orderController));
router.get("/prefix", controllers.extrasController.getPrefix);
router.get("/system", controllers.extrasController.showConfig);
router.get("/chat-admin", (req, res, next) => {
  res.render("chat-admin");
});

export = router;
