import log4js from "log4js";
import config from "../config/globals";
const { LOG_FILE_PATH, LOGGER_MODE } = config;

log4js.configure({
  appenders: {
    myLog: { type: "console" },
    myLogFile: { type: "file", filename: LOG_FILE_PATH },
  },
  categories: {
    default: { appenders: ["myLog"], level: "info" },
    info: { appenders: ["myLog"], level: "info" },
    debug: { appenders: ["myLog"], level: "debug" },
    file: { appenders: ["myLogFile"], level: "error" },
  },
});

export const logger = log4js.getLogger(LOGGER_MODE);
