import crypto from "crypto";
import jsonwebtoken, { Jwt } from "jsonwebtoken";
import config from "../config/globals";
import User from "../types/modules/User";
const { JWT_EXPIRATION } = config;

const issueJWT = (user: User) => {
  const currentDate = Math.floor(Date.now() / 1000);
  const payload = {
    sub: user,
    iat: currentDate,
    exp: currentDate + JWT_EXPIRATION,
  };
  const signedToken: any = jsonwebtoken.sign(payload, "secret");
  return { token: `Bearer ${signedToken}` };
};

const validPassword = (password: any, hash: any, salt: any) => {
  const hashVerify = crypto
    .pbkdf2Sync(password, salt, 10000, 64, "sha512")
    .toString("hex");
  return hash === hashVerify;
};

const genPassword = (password: any) => {
  const salt = crypto.randomBytes(32).toString("hex");
  const genHash = crypto
    .pbkdf2Sync(password, salt, 10000, 64, "sha512")
    .toString("hex");

  return { salt: salt, hash: genHash };
};

const jwtVerify = async (token: string) => {
  try {
    const verified: any = await jsonwebtoken.verify(token, "secret");
    return verified;
  } catch (err) {
    return false;
  }
};

export default {
  validPassword,
  genPassword,
  issueJWT,
  jwtVerify,
};
