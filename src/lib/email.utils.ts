const body = (order: any) => `
<body>
<h3>Orden de compra</h3>
<p>email: <strong>${order.email}</strong>
<p>Fecha y hora: <strong>${order.datetime}</strong>
<p><strong>${order.status}</strong>
<table>
  <thead>
    <th>Código</th>
    <th>Titulo</th>
    <th>P.Unitario</th>
    <th>Cantidad</th>
    <th>Total</th>
  </thead>
  <tbody>
    
  ${order.items.map(
    (item: any) =>
      `<tr>
        <td>${item.code}</td>
        <td>${item.title}</td>
        <td>${item.unitPrice}</td>
        <td>${item.qty}</td>
        <td>${item.totalPriceItem}</td>
      </tr>`
  )}
  <tfoot>
    <td></td>
    <td></td>
    <td></td>
    <td>Total</td>
    <td>
      <strong><h2>${order.totalPrice}</h2></strong>
    </td>
  </tbody>
</table>
`;

export default body;
