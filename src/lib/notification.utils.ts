import nodemailer, { Transporter } from "nodemailer";
import config from "../config/globals";
import nodemailerConfig from "../config/nodemailer.config";
import { logger } from "./logger";
const { EMAIL_CONFIG } = config;

const { ETHEREAL_CONFIG, GMAIL_CONFIG } = nodemailerConfig;

class NotificationUtils {
  private transporter: Transporter;
  constructor(emailConfig: any) {
    this.transporter = nodemailer.createTransport(emailConfig);
    logger.info("Creating Notification object");
    logger.debug(emailConfig);
  }

  sendEmail = async (to: string, body: string, subject: string) => {
    logger.info(`Sending email to ${to}, subject: ${subject}`);
    await this.transporter.sendMail({
      from: '"[Mailer] do-not-reply@ecommerce.com"',
      to: to,
      subject: subject,
      html: `<div>${body}</div>`,
    });
  };
}

export const notificationUtils = new NotificationUtils(GMAIL_CONFIG);
