import repositories from "../../dal/repositories";

const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

let opts = {
  ignoreExpireation: false,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: "secret",
};

const { userRepository } = repositories;

const strategy = new JwtStrategy(opts, (payload: any, done: Function) => {
  const { userRepository } = repositories;
  userRepository
    .getByFilter({ _id: payload.sub })
    .then((user) => {
      if (user) {
        done(null, user);
      } else done(null, false);
    })
    .catch((err) => done(err, null));
});

export default (passport: any) => {
  passport.use(strategy);
};
