import { db } from "../dal/db";
import router from "../routes";
import Server from "./server";
import WebSocket from "./websocket";

export const server = new Server(router, new WebSocket(), db);
