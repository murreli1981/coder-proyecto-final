import express, { Application, Request, Response } from "express";
import DB from "../types/Db";
import config from "../config/globals";
import { logger } from "../lib/logger";
import os from "os";
import cors from "cors";

import http from "http";
import cluster from "cluster";

const { SERVER_PORT, SERVER_CLUSTER } = config;

export default class Server {
  private app: Application;
  constructor(private router: any, private socket: any, private db: DB) {
    this.app = express();
    this.config();
  }

  private config() {
    //this.app.use(cors());
    this.app.use(this.router);
    //templates
    this.app.set("views", "./src/views");
    this.app.set("view engine", "pug");
    this.app.set("view engine", "ejs");
    this.app.use((error: any, req: Request, res: Response, next: Function) => {
      logger.error(
        `[${this.constructor.name}] ${JSON.stringify(error, null, "\t")}`
      );
      res.status(error.statusCode).send(error.msg);
    });
  }

  async start() {
    const server = http.createServer(this.app);
    this.socket.setWebSocket(server);

    if (!cluster.isWorker && SERVER_CLUSTER) {
      logger.info("CLUSTER MODE");
      logger.debug(`${process.pid} parent process`);
      for (let i = 0; i < os.cpus().length; i++) cluster.fork();
    } else {
      await this.db.getConnection();
      await this.socket.start();
      await server.listen(SERVER_PORT, () => {
        logger.info(`Server listening in port: ${SERVER_PORT}`);
      });
    }
  }
}
