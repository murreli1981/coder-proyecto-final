import { Server, Socket } from "socket.io";
import { logger } from "../lib/logger";
import services from "../services";
const { notificationService, messageService } = services;
import globals from "../config/globals";
import moment from "moment";
import Message from "../types/modules/Message";

const { ADMIN_MAILBOX, HOSTNAME_DEV, HOSTNAME_PROD, SERVER_MODE } = globals;

export default class WebSocket {
  private io: any = null;
  private static readonly MESSAGE_SIGNAL: string = "message";
  private static readonly JOIN_ROOM_SIGNAL: string = "joinRoom";
  private static readonly CONNECTION_SIGNAL: string = "connection";
  private static readonly DISCONNECT_SIGNAL: string = "disconnect";

  setWebSocket = (server: any) => {
    this.io = new Server(server);
  };

  start = () => {
    const rooms: any = {};

    this.io.on(WebSocket.CONNECTION_SIGNAL, (socket: Socket) => {
      logger.info("[WEBSOCKET] client connected");
      const { id } = socket;
      logger.debug(`[WEBSOCKET] Socket id: ${id}`);
      !rooms[id] && rooms[id];
      socket.on(WebSocket.JOIN_ROOM_SIGNAL, async ({ room, user }) => {
        let messages: any;
        messages = await messageService.getAll({ room });
        rooms[id] = room;
        socket.join(room);
        logger.debug(`[WEBSOCKET] user entered in room: ${room}`);

        const subject = "Chat iniciado";
        const body = `<div>Para iniciar la conversación con el cliente hacé click en el link</div><p></p><div>${
          SERVER_MODE === "production" ? HOSTNAME_PROD : HOSTNAME_DEV
        }/chat-admin?room=${room}</div>`;

        user !== "Admin" &&
          notificationService.notify(ADMIN_MAILBOX, body, subject);
        logger.debug(
          `[WEBSOCKET] chat admin available in server:port/chat-admin?room=${room}`
        );
        logger.debug(
          `[WEBSOCKET] Active sockets and chat rooms: ${JSON.stringify(
            rooms,
            null,
            "\t"
          )}`
        );
        logger.info(`[WEBSOCKET]: Retrieving messages: ${messages.length}`);
        logger.debug(`${JSON.stringify(messages, null, "\t")}`);

        messages.length > 0 &&
          this.io.to(rooms[id]).emit(WebSocket.MESSAGE_SIGNAL, {
            firstLoad: true,
            messages: messages,
          });
        logger.info(
          `${JSON.stringify({ firstLoad: true, messages: messages })}`
        );

        socket.broadcast
          .to(rooms[id])
          .emit(WebSocket.MESSAGE_SIGNAL, `${user} has joined`);
        logger.info(
          `[WEBSOCKET]: ${user} has joined in chat room ${rooms[id]}`
        );
      });

      socket.on(WebSocket.MESSAGE_SIGNAL, async ({ user, message }) => {
        const date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        const messageToSave: Message = {
          datetime: date + "",
          username: user,
          room: rooms[id],
          message: message,
        };
        await messageService.create(messageToSave);
        this.io
          .to(rooms[id])
          .emit(WebSocket.MESSAGE_SIGNAL, `[${user}] ${date} - ${message}`);
      });

      socket.on(WebSocket.DISCONNECT_SIGNAL, () => {
        this.io.to(rooms[id]).emit(WebSocket.MESSAGE_SIGNAL, "user left");
        delete rooms[id];
        logger.debug(`[WEBSOCKET] removing id: ${id} from active rooms list`);
      });
    });
  };
}
