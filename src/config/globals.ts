import { config } from "dotenv";
import path from "path/posix";
config();

export default {
  MONGO_PROD: process.env.MONGO_PROD || "",
  MONGO_DEV: process.env.MONGO_DEV || "",
  SERVER_MODE: process.argv[2] || "development",
  SERVER_PORT: process.env.SERVER_PORT || 3000,
  SERVER_CLUSTER: process.env.SERVER_CLUSTER || null,
  LOG_FILE_PATH: process.env.LOG_FILE_PATH || "./logs/error.log",
  LOGGER_MODE: process.env.LOGGER_MODE || "default",
  EMAIL_CONFIG: process.env.GMAIL_CONFIG,
  DB: process.env.DB,
  JWT_EXPIRATION: process.env.JWT_EXPIRATION
    ? parseInt(process.env.JWT_EXPIRATION)
    : 60 * 60 * 24 * 7, //default time 1 week
  ADMIN_MAILBOX: process.env.ADMIN_MAILBOX || "email@email.com",
  HOSTNAME_PROD: process.env.HOSTNAME_PROD || "",
  HOSTNAME_DEV:
    process.env.HOSTNAME_DEV ||
    "http://localhost" + ":" + process.env.SERVER_PORT ||
    3000,
};
