import UserModel from "./user";
import ProductModel from "./product";
import ShoppingCartModel from "./shopping-cart";
import MessageModel from "./message";
import OrderModel from "./order";

export default {
  MessageModel,
  UserModel,
  ProductModel,
  ShoppingCartModel,
  OrderModel,
};
