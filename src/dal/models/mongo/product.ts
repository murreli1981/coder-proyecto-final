import { Schema, model } from "mongoose";

const productSchema: Schema = new Schema(
  {
    title: String,
    description: String,
    code: {
      type: String,
      unique: true,
      dropDups: true,
    },
    thumbnail: String,
    price: Number,
    stock: Number,
  },
  { versionKey: false }
);

productSchema.set("toJSON", { virtuals: true });

export default model("Product", productSchema);
