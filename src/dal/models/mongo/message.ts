import { Schema, model } from "mongoose";

const messageSchema: Schema = new Schema(
  {
    username: String,
    message: String,
    room: String,
    datetime: String,
  },
  { versionKey: false }
);

messageSchema.set("toJSON", { virtuals: true });

export default model("Message", messageSchema);
