import { Schema, model } from "mongoose";
import { Item } from "../../../types/modules/Order";

const orderSchema: Schema = new Schema(
  {
    items: [
      {
        _id: false,
        title: String,
        qty: Number,
        code: String,
        unitPrice: Number,
        totalPriceItem: Number,
      },
    ],
    email: String,
    datetime: Date,
    status: String,
    totalPrice: Number,
  },
  { versionKey: false }
);

orderSchema.set("toJSON", { virtuals: true });

export default model("Order", orderSchema);
