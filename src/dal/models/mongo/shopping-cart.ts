import { Schema, model } from "mongoose";

const shoppingCartSchema: Schema = new Schema(
  {
    timestamp: String,
    products: [
      {
        type: Schema.Types.ObjectId,
        ref: "Product",
      },
    ],
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  },
  { versionKey: false }
);

shoppingCartSchema.set("toJSON", { virtuals: true });

export default model("ShoppingCart", shoppingCartSchema);
