import { Schema, model } from "mongoose";

const userSchema: Schema = new Schema(
  {
    username: {
      type: String,
      unique: true,
      dropDups: true,
    },
    password: String,
    name: String,
    address: String,
    age: Number,
    phone: String,
    salt: String,
    admin: Boolean,
  },
  { versionKey: false }
);

userSchema.set("toJSON", { virtuals: true });

export default model("User", userSchema);
