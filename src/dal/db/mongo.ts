import { connect, connection } from "mongoose";
import DB from "../../types/Db";
import { logger } from "../../lib/logger";

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

export class MongoDB implements DB {
  constructor(private connection: string) {}
  public async getConnection() {
    await connect(this.connection, options);
    logger.info("DB Connected");
    logger.debug(connection);
  }
}
