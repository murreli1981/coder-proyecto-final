import { MongoDB } from "./mongo";
import globals from "../../config/globals";
import DB from "../../types/Db";

export let db: DB;
const { SERVER_MODE } = globals;

if (globals.DB === "mongo") {
  if (SERVER_MODE === "production") {
    db = new MongoDB(globals.MONGO_PROD);
  } else if (SERVER_MODE === "development") {
    db = new MongoDB(globals.MONGO_DEV);
  }
} else {
  if (SERVER_MODE === "production") {
    db = new MongoDB(globals.MONGO_PROD);
  } else if (SERVER_MODE === "development") {
    db = new MongoDB(globals.MONGO_DEV);
  }
}
