import repositories from "./mongo"; //repositorio intercambiable
import mongoModels from "../models/mongo"; //model intercambiable

import config from "../../config/globals";
import Repository from "../../types/Repository";

let userRepository,
  productRepository,
  shoppingCartRepository,
  messageRepository,
  orderRepository: Repository;

//Factory para persistencia
const { DB } = config;
if (DB === "mongo") {
  const {
    UserRepository,
    ProductRepository,
    ShoppingCartRepository,
    MessageRepository,
    OrderRepository,
  } = repositories;

  const {
    UserModel,
    ProductModel,
    ShoppingCartModel,
    MessageModel,
    OrderModel,
  } = mongoModels;

  userRepository = new UserRepository(UserModel);
  productRepository = new ProductRepository(ProductModel);
  shoppingCartRepository = new ShoppingCartRepository(ShoppingCartModel);
  messageRepository = new MessageRepository(MessageModel);
  orderRepository = new OrderRepository(OrderModel);
  /**
   * if (DB === "otro") { acá asignaria otro origen de repositorios}
   */
} else {
  //dejo un tipo default que tambien va contra mongo
  const {
    UserRepository,
    ProductRepository,
    ShoppingCartRepository,
    MessageRepository,
    OrderRepository,
  } = repositories;

  const {
    UserModel,
    ProductModel,
    ShoppingCartModel,
    MessageModel,
    OrderModel,
  } = mongoModels;

  userRepository = new UserRepository(UserModel);
  productRepository = new ProductRepository(ProductModel);
  shoppingCartRepository = new ShoppingCartRepository(ShoppingCartModel);
  messageRepository = new MessageRepository(MessageModel);
  orderRepository = new OrderRepository(OrderModel);
}

export default {
  userRepository,
  productRepository,
  shoppingCartRepository,
  messageRepository,
  orderRepository,
};
