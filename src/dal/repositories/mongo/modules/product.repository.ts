import { logger } from "../../../../lib/logger";
import BaseRepository from "../base.respository";

export default class ProductRepository extends BaseRepository {
  constructor(model: any) {
    super(model);
  }
  getAll(filter?: any): any {
    try {
      if (filter) {
        const preparedFilter = this.prepareQuery(filter);
        logger.debug(preparedFilter);
        return super.getAll(preparedFilter);
      }
      return super.getAll();
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }

  private prepareQuery = (filter: any) => {
    let query: any = {};

    if (filter.title) query = { ...query, ...{ title: filter.title } };
    if (filter.code) query = { ...query, ...{ code: Number(filter.code) } };
    if (filter.price)
      query = { ...query, ...{ price: { $gte: Number(filter.price) } } };
    if (filter.priceMax)
      query.price = { ...query.price, $lte: Number(filter.priceMax) };
    if (filter.stock)
      query = { ...query, ...{ stock: { $gte: Number(filter.stock) } } };
    if (filter.stockMax)
      query.stock = { ...query.stock, $lte: Number(filter.stockMax) };

    return query;
  };
}
