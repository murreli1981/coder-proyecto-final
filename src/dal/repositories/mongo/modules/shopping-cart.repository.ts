import { logger } from "../../../../lib/logger";
import ShoppingCart from "../../../../types/modules/ShoppingCart";
import BaseRepository from "../base.respository";

export default class ShoppingCartRepository extends BaseRepository {
  constructor(model: any) {
    super(model);
  }

  async getAll(filter?: any) {
    try {
      const shoppingCarts: ShoppingCart[] = await this.model
        .find(filter)
        .populate({ path: "user", model: "User" })
        .populate({ path: "products", model: "Product" })
        .exec();
      return shoppingCarts;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }

  async getById(id: String) {
    logger.info(`[${this.constructor.name}] geting document using id: ${id} `);
    try {
      const shoppingCart = await this.model
        .findById(id)
        .populate({ path: "user", model: "User" })
        .populate({ path: "products", model: "Product" })
        .exec();
      return shoppingCart;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }

  async getByFilter(filter: any) {
    logger.info(
      `[${
        this.constructor.name
      }] geting document using filter: ${JSON.stringify(filter)} `
    );
    try {
      const shoppingCart = await this.model
        .findOne(filter)
        .populate({ path: "user", models: "User" })
        .populate({ path: "products", model: "Product" })
        .exec();
      return shoppingCart || {};
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }
}
