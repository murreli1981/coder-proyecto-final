import BaseRepository from "../base.respository";

export default class UserRepository extends BaseRepository {
  constructor(model: any) {
    super(model);
  }
}
