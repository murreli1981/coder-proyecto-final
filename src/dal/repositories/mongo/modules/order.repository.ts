import { logger } from "../../../../lib/logger";
import BaseRepository from "../base.respository";
export default class OrderRepository extends BaseRepository {
  constructor(model: any) {
    super(model);
  }
  async getAll(filter?: any): Promise<any> {
    try {
      return await this.model.find(filter).lean();
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }

  async getById(id: String) {
    try {
      const data = await this.model.findById(id).lean();
      return data;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }

  async getByFilter(filter: any) {
    try {
      const data = await this.model.findOne(filter).lean();
      return data;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }
}
