import { logger } from "../../../lib/logger";

export default abstract class BaseRepository {
  protected model;
  constructor(model: any) {
    this.model = model;
  }

  async getAll(filter?: any): Promise<any> {
    try {
      return await this.model.find(filter);
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }

  async getById(id: String) {
    try {
      const data = await this.model.findById(id);
      return data;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }

  async getByFilter(filter: any) {
    try {
      const data = await this.model.findOne(filter);
      return data;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }
  async save(payload: any) {
    try {
      const response = await this.model.create(payload);
      return response;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }
  async update(id: String, payload: any) {
    try {
      const response = await this.model.findByIdAndUpdate(id, payload);
      return response;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }
  async delete(id: String) {
    try {
      const response = await this.model.findByIdAndDelete(id);
      return response;
    } catch (err) {
      logger.error(`[${this.constructor.name}] ${err}`);
    }
  }
}
