import ProductRepository from "./modules/product.repository";
import ShoppingCartRepository from "./modules/shopping-cart.repository";
import UserRepository from "./modules/user.repository";
import MessageRepository from "./modules/message.repository";
import OrderRepository from "./modules/order.repository";

export default {
  UserRepository,
  ProductRepository,
  ShoppingCartRepository,
  MessageRepository,
  OrderRepository,
};
