import ProductService from "./modules/product.service";
import ShoppingCartService from "./modules/shopping-cart.service";
import UserService from "./modules/user.service";
import MessageService from "./modules/message.service";
import OrderService from "./modules/order.service";
import ExtrasService from "./extras.service";
import NotificationService from "./notification.service";

import { notificationUtils } from "../lib/notification.utils";

import schemas from "../types/schemas";
import repositories from "../dal/repositories";

const {
  userRepository,
  productRepository,
  shoppingCartRepository,
  messageRepository,
  orderRepository,
} = repositories;

export default {
  userService: new UserService(
    userRepository,
    schemas.UserSchema,
    schemas.UsersSchema
  ),
  productService: new ProductService(
    productRepository,
    schemas.ProductSchema,
    schemas.ProductsSchema
  ),
  shoppingCartService: new ShoppingCartService(
    shoppingCartRepository,
    schemas.ShoppingCartSchema,
    schemas.ShoppingCartsSchema
  ),
  messageService: new MessageService(
    messageRepository,
    schemas.MessageSchema,
    schemas.MessagesSchema
  ),
  orderService: new OrderService(
    orderRepository,
    schemas.OrderSchema,
    schemas.OrdersSchema
  ),
  extrasService: new ExtrasService(),
  notificationService: new NotificationService(notificationUtils),
};
