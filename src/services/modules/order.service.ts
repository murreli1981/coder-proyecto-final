import { Schema } from "joi";
import Order from "../../types/modules/Order";
import ShoppingCart from "../../types/modules/ShoppingCart";
import Repository from "../../types/Repository";
import BaseService from "./base.service";

export default class OrderService extends BaseService {
  constructor(
    repository: Repository,
    singleSchema: Schema,
    arraySchema: Schema
  ) {
    super(repository, singleSchema, arraySchema);
  }
  async create(document: any): Promise<{}> {
    let order: Order;
    order = { ...document, datetime: new Date() };
    return await super.create(order);
  }
}
