import { Schema } from "joi";
import Product from "../../types/modules/Product";
import ShoppingCart from "../../types/modules/ShoppingCart";
import Repository from "../../types/Repository";
import BaseService from "./base.service";

export default class ShoppingCartService extends BaseService {
  constructor(
    repository: Repository,
    singleSchema: Schema,
    arraySchema: Schema
  ) {
    super(repository, singleSchema, arraySchema);
  }

  async getByFilter(filter: any): Promise<{}> {
    return this.repository.getByFilter(filter);
  }

  async create(document: any) {
    let shoppingCart: ShoppingCart = {
      ...document,
      timestamp: `${Date.now()}`,
    };
    return super.create(shoppingCart);
  }

  async prepareCheckout(id: any) {
    try {
      const shoppingCart: any = await super.getById(id);
      if (shoppingCart && shoppingCart.products.length > 0) {
        const { arr: items, total: totalPrice } = calculateItemsList(
          shoppingCart.products
        );
        const email = shoppingCart.user.username;
        return { items, totalPrice, email };
      } else return null;
    } catch (error) {}
  }
}

const calculateItemsList = (items: Product[]): any => {
  let total: number = 0;
  let freq: any = {};
  items.map((prod: Product) => {
    total += prod.price;
    if (freq[prod.id]) {
      freq[prod.id].qty++;
      freq[prod.id].totalPriceItem += prod.price;
    } else
      freq[prod.id] = {
        qty: 1,
        unitPrice: prod.price,
        code: prod.code,
        title: prod.title,
        totalPriceItem: prod.price,
      };
  });
  const arr: any[] = [...Object.values(freq)];
  return { arr, total };
};
