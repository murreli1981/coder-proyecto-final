import { Schema } from "joi";
import { logger } from "../../lib/logger";
import Repository from "../../types/Repository";
const joiRules = {
  stripUnknown: true,
  abortEarly: false,
};

export default abstract class BaseService {
  constructor(
    protected repository: Repository,
    protected singleSchema: Schema,
    protected arraySchema: Schema
  ) {}
  async getAll(filter?: any) {
    logger.info(
      `[${
        this.constructor.name
      }] getting documents using filter: ${JSON.stringify(filter)}`
    );
    const response = await this.repository.getAll(filter);
    const { error } = this.arraySchema.validate(response, joiRules);
    if (error) {
      logger.error(
        `[${this.constructor.name}] schema validation error: ${error}`
      );
      return { statusCode: 400, msg: error.details };
    }
    logger.debug(response);
    return response;
  }

  async getById(id: string) {
    logger.info(`[${this.constructor.name}] getting document by id: ${id}`);
    const response = await this.repository.getById(id);
    if (response) {
      const { error } = this.singleSchema.validate(response, joiRules);
      if (error) {
        logger.error(
          `[${this.constructor.name}] schema validation error: ${error}`
        );
        return { statusCode: 400, msg: error.details };
      }
    }
    logger.debug(response);
    return response;
  }

  async getByFilter(filter: any) {
    logger.info(
      `[${
        this.constructor.name
      }] getting document using filter: ${JSON.stringify(filter)}`
    );
    const response = await this.repository.getByFilter(filter);
    if (response) {
      const { error } = this.singleSchema.validate(response, joiRules);
      if (error) {
        logger.error(
          `[${this.constructor.name}] schema validation error: ${error}`
        );
        return { statusCode: 400, msg: error.details };
      }
    }
    logger.debug(response);
    return response;
  }

  async create(document: any) {
    logger.info(`[${this.constructor.name}] saving document`);
    logger.debug(`[${this.constructor.name}] content to create`);
    logger.debug(document);

    const { error } = this.singleSchema.validate(document, {
      abortEarly: false,
    });
    if (error) {
      logger.error(
        `[${this.constructor.name}] schema validation error: ${error}`
      );
      const err = { statusCode: 400, msg: error.details };
      return err;
    }

    const response = await this.repository.save(document);
    logger.debug(`Response: ${JSON.stringify(response, null, "\t")}`);
    return response;
  }

  async update(id: string, toModify: any) {
    logger.info(`[${this.constructor.name}] updating document: ${id}`);
    logger.debug(`[${this.constructor.name}] content to update`);
    logger.debug(toModify);
    const response = await this.repository.update(id, toModify);
    logger.debug(response);
    return response;
  }

  async delete(id: string) {
    logger.info(`[${this.constructor.name}] deleting document ${id}`);
    const response = await this.repository.delete(id);
    logger.debug(response);
    return response;
  }
}
