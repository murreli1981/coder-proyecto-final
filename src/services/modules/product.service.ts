import { Schema } from "joi";
import Repository from "../../types/Repository";
import BaseService from "./base.service";

export default class ProductService extends BaseService {
  constructor(
    repository: Repository,
    singleSchema: Schema,
    arraySchema: Schema
  ) {
    super(repository, singleSchema, arraySchema);
  }
}
