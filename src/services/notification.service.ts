import Notification from "../types/Notification";

export default class NotificationService {
  constructor(private readonly notificationObj: Notification) {}

  notify = (to: string, body: string, subject: string) => {
    this.notificationObj.sendEmail(to, body, subject);
  };
}
