import { prefixdb } from "../dal/db/prefix.db";
import os from "os";
import config from "../config/globals";

export default class ExtrasService {
  private readonly prefixes = prefixdb;
  getPrefixes() {
    return this.prefixes;
  }
  getServerStats = () => {
    const info = {
      "Argumentos de entrada": process.argv.slice(2),
      "Sistema Operativo": process.platform,
      "Numero de CPUs": os.cpus().length,
      "Version de NodeJS": process.version,
      "Uso de memoria": JSON.stringify(process.memoryUsage()),
      "Path de ejecución": process.execPath,
      "Process Id": process.pid,
      "Carpeta corriente": process.cwd(),
      "Modo de servidor": config.SERVER_MODE,
      "Base de datos": config.DB,
    };
    return info;
  };
}
