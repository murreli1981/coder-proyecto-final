export default interface Repository {
  getAll: (filter?: {}) => {};
  getById: (id: string) => {};
  getByFilter: (filter: {}) => {};
  save: (document: any) => {};
  update: (id: string, payload: {}) => {};
  delete: (id: string) => {};
}
