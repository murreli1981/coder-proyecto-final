import Joi from "joi";
const { object, string, array, any, number } = Joi.types();

const ProductSchema = object.keys({
  _id: any,
  id: any,
  title: string.max(50).min(1).required(),
  description: string.max(255).min(1).required(),
  code: number.max(10000).required(),
  thumbnail: string.uri().required(),
  price: number.min(1).max(99999999).required(),
  stock: number.min(0).max(1000).required(),
});

const ProductsSchema = array.items(ProductSchema).min(0);

export { ProductSchema, ProductsSchema };
