import Joi from "joi";
const { object, string, array, any, number } = Joi.types();

const MessageSchema = object.keys({
  _id: any,
  id: any,
  username: string.min(1).required(),
  message: string.min(1).max(255).required(),
  room: string.required(),
  datetime: string,
});

const MessagesSchema = array.items(MessageSchema).min(0);

export { MessageSchema, MessagesSchema };
