import Joi from "joi";
const { object, string, array, any, number, date } = Joi.types();

const OrderSchema = object.keys({
  id: any,
  _id: any,
  items: array.items({
    title: string.required(),
    qty: number.min(0),
    code: string.required(),
    unitPrice: number.min(0).required(),
    totalPriceItem: number.min(0).required(),
  }),
  email: string.email().required(),
  datetime: date.required(),
  status: string,
  totalPrice: number,
});

const OrdersSchema = array.items(OrderSchema).min(0);

export { OrderSchema, OrdersSchema };
