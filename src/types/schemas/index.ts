import { ProductSchema, ProductsSchema } from "./product.schema";
import { ShoppingCartSchema, ShoppingCartsSchema } from "./shoppingCart.schema";
import { UserSchema, UsersSchema } from "./user.schema";
import { MessageSchema, MessagesSchema } from "./message.schema";
import { OrderSchema, OrdersSchema } from "./order.schema";

export default {
  ProductSchema,
  ProductsSchema,
  ShoppingCartSchema,
  ShoppingCartsSchema,
  UserSchema,
  UsersSchema,
  MessageSchema,
  MessagesSchema,
  OrderSchema,
  OrdersSchema,
};
