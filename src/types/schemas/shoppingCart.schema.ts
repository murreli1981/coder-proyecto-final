import Joi from "joi";
const { object, string, array, any } = Joi.types();

const ShoppingCartSchema = object.keys({
  id: any,
  _id: any,
  timestamp: string.min(13).max(13).required(),
  products: array.required(),
  user: any.required(),
});

const ShoppingCartsSchema = array.items(ShoppingCartSchema).min(0);

export { ShoppingCartSchema, ShoppingCartsSchema };
