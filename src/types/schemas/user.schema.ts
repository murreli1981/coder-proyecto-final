import Joi from "joi";
const { string, number, boolean, object, array, any } = Joi.types();

const UserSchema = object.keys({
  _id: any,
  id: any,
  username: string.email().required().lowercase(),
  password: string.required(),
  salt: string.required(),
  name: string.required(),
  address: string.required().required(),
  age: number.min(18).max(100).required(),
  phone: string.min(0).max(15).required(),
  admin: boolean.required(),
});

const UsersSchema = array.items(UserSchema).min(0);

export { UserSchema, UsersSchema };
