import { Request, Response } from "express";

export default interface Controller {
  getAll: (req: Request, res: Response, next: Function) => {};
  getById: (req: Request, res: Response, next: Function) => {};
  getByFilter: (req: Request, res: Response, next: Function) => {};
  create: (req: Request, res: Response, next: Function) => {};
  update: (req: Request, res: Response, next: Function) => {};
  delete: (req: Request, res: Response, next: Function) => {};
}
