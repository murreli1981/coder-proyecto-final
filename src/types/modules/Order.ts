export interface Item {
  title: string;
  qty: number;
  code: string;
  unitPrice: number;
  totalPriceItem: number;
}

export default interface Order {
  id?: any;
  _id?: any;
  items: Array<Item>;
  email: string;
  datetime: Date;
  status: string;
  totalPrice: number;
}
