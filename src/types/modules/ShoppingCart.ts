import Product from "./Product";
import User from "./User";

export default interface ShoppingCart {
  id?: any;
  timestamp: string;
  products: Product[];
  user: User;
}
