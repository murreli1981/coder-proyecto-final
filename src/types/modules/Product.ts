export default interface Product {
  id?: any;
  title: string;
  description: string;
  code: string;
  thumbnail: string;
  price: number;
  stock: number;
}
