export default interface Message {
  id?: any;
  username: string;
  message: string;
  room: string;
  datetime: string;
}
