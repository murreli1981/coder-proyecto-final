export default interface User {
  id?: string;
  username: string;
  password: string;
  salt: string;
  name: string;
  address: string;
  age: number;
  phone: string;
  admin: boolean;
}
