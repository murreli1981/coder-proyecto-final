export default interface Notification {
  sendEmail: (to: string, body: string, subject: string) => {};
}
