export default interface Service {
  getAll: (filter?: {}) => any;
  getById: (id: string) => any;
  getByFilter: (filter: {}) => any;
  create: (document: any) => any;
  update: (id: string, toModify: {}) => any;
  delete: (id: string) => any;
}
