import { Request, Response } from "express";

export class ExtrasController {
  constructor(private service: any) {}

  getPrefix = (req: Request, res: Response, next: Function) => {
    const prefixes = this.service.getPrefixes();
    res.status(200).json(prefixes);
  };

  showConfig = (req: Request, res: Response, next: Function) => {
    const stats = this.service.getServerStats();
    res.status(200).render("config.pug", { info: stats });
  };
}
