import { Request, Response } from "express";
import utils from "../../lib/jwt.utils";
import NotificationService from "../../services/notification.service";
import User from "../../types/modules/User";
import Service from "../../types/Service";
import { BaseController } from "./base.controller";
import config from "../../config/globals";
const { ADMIN_MAILBOX } = config;

export class UserController extends BaseController {
  constructor(service: Service, private notificationService: any) {
    super(service);
  }

  create = async (req: Request, res: Response, next: Function) => {
    const saltHash = utils.genPassword(req.body.password);
    const salt = saltHash.salt;
    const hash = saltHash.hash;

    const user: User = {
      ...req.body,
      password: hash,
      salt: salt,
      admin: false,
    };

    const createdUser: any = await this.service.create(user);
    const jwt = utils.issueJWT(createdUser);
    const body = `<h3>${createdUser.username}</h3><p>${createdUser._id}</p><p>${createdUser.name}</p>`;
    createdUser._id &&
      this.notificationService.notify(
        ADMIN_MAILBOX,
        body,
        "Nuevo usuario registrado!"
      );
    res.json({
      success: true,
      user: user,
      token: jwt.token,
    });
  };

  login = async (req: Request, res: Response, next: Function) => {
    let user: User;
    user = await this.service.getByFilter({
      username: req.body.username,
    });

    if (!user) res.status(401).json({ success: false, msg: "user not found" });
    else {
      const isValid = utils.validPassword(
        req.body.password,
        user.password,
        user.salt
      );

      if (isValid) {
        const tokenObj = utils.issueJWT(user);
        res.status(200).json({
          user,
          token: tokenObj.token,
        });
      } else {
        res.status(401).json({ success: false, msg: "wrong password" });
      }
    }
  };

  whoami = async (req: Request, res: Response, next: Function) => {
    const bearerHeader: any = req.headers["authorization"];
    if (bearerHeader) {
      const token = bearerHeader.split(" ")[1];
      const content = await utils.jwtVerify(token);

      if (content) {
        res.status(200).json({ content });
      }
    } else {
      res.status(401).json({ msg: "unauthorized" });
    }
  };
}
