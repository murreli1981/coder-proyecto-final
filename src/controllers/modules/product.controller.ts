import Service from "../../types/Service";
import { BaseController } from "./base.controller";

export class ProductController extends BaseController {
  constructor(service: Service) {
    super(service);
  }
}
