import { Request, Response } from "express";
import { logger } from "../../lib/logger";
import Controller from "../../types/Controller";
import Service from "../../types/Service";

export abstract class BaseController implements Controller {
  constructor(protected readonly service: Service | any) {}

  getAll = async (req: Request, res: Response, next: Function) => {
    const data: any = await this.service.getAll(req.query || null);
    this.handleResponse(data, res, next);
  };

  getById = async (req: Request, res: Response, next: Function) => {
    const data: any = await this.service.getById(req.params.id);
    this.handleResponse(data, res, next);
  };

  create = async (req: Request, res: Response, next: Function) => {
    const data: any = await this.service.create(req.body);
    this.handleResponse(data, res, next);
  };

  update = async (req: Request, res: Response, next: Function) => {
    const data: any = await this.service.update(req.params.id, req.body);
    this.handleResponse(data, res, next);
  };

  delete = async (req: Request, res: Response, next: Function) => {
    const data: any = await this.service.delete(req.params.id);
    this.handleResponse(data, res, next);
  };

  getByFilter = async (req: Request, res: Response, next: Function) => {
    const data: any = await this.service.getByFilter(req.query);
    this.handleResponse(data, res, next);
  };

  protected handleResponse(data: any, res: Response, next: Function): void {
    if (data) {
      data.statusCode ? next(data) : res.json(data);
    } else next({ statusCode: 404, msg: "content not found" });
  }
}
