import { Request, Response } from "express";
import globals from "../../config/globals";
import body from "../../lib/email.utils";
import Order, { Item } from "../../types/modules/Order";
import Service from "../../types/Service";
import { BaseController } from "./base.controller";

export class ShoppingCartController extends BaseController {
  constructor(
    service: Service | any,
    private orderService: Service,
    private notificationService: any
  ) {
    super(service);
  }

  checkout = async (req: Request, res: Response, next: Function) => {
    const orderElements = await this.service.prepareCheckout(req.params.id);
    if (orderElements) {
      const { error, items, totalPrice, email } = orderElements;
      const orderToProcess: Order = {
        items,
        email,
        datetime: new Date(),
        status: "accepted",
        totalPrice,
      };
      const data: Order | any = await this.orderService.create(orderToProcess);
      data.id && (await this.service.delete(req.params.id));
      this.notificationService.notify(
        globals.ADMIN_MAILBOX,
        body(data),
        "Order recibida"
      );
      this.handleResponse(data, res, next);
    } else {
      this.handleResponse(orderElements, res, next);
    }
  };
}
