import { Request, Response } from "express";
import Service from "../../types/Service";
import { BaseController } from "./base.controller";

export class OrderController extends BaseController {
  constructor(service: any) {
    super(service);
  }
}
