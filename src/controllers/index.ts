import { ProductController } from "./modules/product.controller";
import { ShoppingCartController } from "./modules/shopping-cart.controller";
import { UserController } from "./modules/user.controller";
import { MessageController } from "./modules/message.controller";
import { ExtrasController } from "./extras.controller";
import { OrderController } from "./modules/order.controller";

import services from "../services";

export default {
  productController: new ProductController(services.productService),
  userController: new UserController(
    services.userService,
    services.notificationService
  ),
  shoppingCartController: new ShoppingCartController(
    services.shoppingCartService,
    services.orderService,
    services.notificationService
  ),
  messageController: new MessageController(services.messageService),
  extrasController: new ExtrasController(services.extrasService),
  orderController: new OrderController(services.orderService),
};
