import jwtStrategy from "../lib/auth/passport-jwt";
import passport from "passport";

jwtStrategy(passport);

export const secured = passport.authenticate("jwt", { session: false });
