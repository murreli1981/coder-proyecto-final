import { Request, Response } from "express";
import User from "../types/modules/User";

export const admin = (req: Request, res: Response, next: Function) => {
  let user: any;
  if (req.user) {
    user = req.user;
    user.admin === true
      ? next()
      : res.status(401).json({ msg: "unauthorized" });
  } else {
    res.status(401).json({ msg: "user not exists" });
  }
};
