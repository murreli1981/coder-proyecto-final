# API REST E-Commerce

Coder house - backend - 13440

</br>
</br>

## Introduccion

Implementación de un ecommerce en node utilizando el siguiente stack

Core:

- Typescript
- Express
- Mongoose

Librerias adicionales:

- cors
- passport
- jsonwebtoken
- nodemailer
- log4js
- dotenv
- socket.io
- moment
- pug
- ejs

## Arbol del proyecto

```
.
├── README.md
├── logs
│   └── error.log
├── nodemon.json
├── package-lock.json
├── package.json
├── src
│   ├── config
│   │   ├── globals.ts
│   │   └── nodemailer.config.ts
│   ├── controllers
│   │   ├── extras.controller.ts
│   │   ├── index.ts
│   │   └── modules
│   │       ├── base.controller.ts
│   │       ├── message.controller.ts
│   │       ├── order.controller.ts
│   │       ├── product.controller.ts
│   │       ├── shopping-cart.controller.ts
│   │       └── user.controller.ts
│   ├── dal
│   │   ├── db
│   │   │   ├── index.ts
│   │   │   ├── mongo.ts
│   │   │   └── prefix.db.ts
│   │   ├── models
│   │   │   └── mongo
│   │   │       ├── index.ts
│   │   │       ├── message.ts
│   │   │       ├── order.ts
│   │   │       ├── product.ts
│   │   │       ├── shopping-cart.ts
│   │   │       └── user.ts
│   │   └── repositories
│   │       ├── index.ts
│   │       └── mongo
│   │           ├── base.respository.ts
│   │           ├── index.ts
│   │           └── modules
│   │               ├── message.repository.ts
│   │               ├── order.repository.ts
│   │               ├── product.repository.ts
│   │               ├── shopping-cart.repository.ts
│   │               └── user.repository.ts
│   ├── index.ts
│   ├── lib
│   │   ├── auth
│   │   │   └── passport-jwt.ts
│   │   ├── email.utils.ts
│   │   ├── jwt.utils.ts
│   │   ├── logger.ts
│   │   └── notification.utils.ts
│   ├── middlewares
│   │   ├── admin.middleware.ts
│   │   └── authentication.middleware.ts
│   ├── routes
│   │   ├── index.ts
│   │   ├── modules
│   │   │   ├── message.routes.ts
│   │   │   ├── order.routes.ts
│   │   │   ├── product.routes.ts
│   │   │   ├── shopping-cart.routes.ts
│   │   │   └── user.routes.ts
│   │   └── router.ts
│   ├── server
│   │   ├── index.ts
│   │   ├── server.ts
│   │   └── websocket.ts
│   ├── services
│   │   ├── extras.service.ts
│   │   ├── index.ts
│   │   ├── modules
│   │   │   ├── base.service.ts
│   │   │   ├── message.service.ts
│   │   │   ├── order.service.ts
│   │   │   ├── product.service.ts
│   │   │   ├── shopping-cart.service.ts
│   │   │   └── user.service.ts
│   │   └── notification.service.ts
│   ├── types
│   │   ├── Controller.ts
│   │   ├── Db.ts
│   │   ├── Notification.ts
│   │   ├── Repository.ts
│   │   ├── Service.ts
│   │   ├── modules
│   │   │   ├── Message.ts
│   │   │   ├── Order.ts
│   │   │   ├── Product.ts
│   │   │   ├── ShoppingCart.ts
│   │   │   └── User.ts
│   │   └── schemas
│   │       ├── index.ts
│   │       ├── message.schema.ts
│   │       ├── order.schema.ts
│   │       ├── product.schema.ts
│   │       ├── shoppingCart.schema.ts
│   │       └── user.schema.ts
│   └── views
│       ├── chat-admin.ejs
│       └── config.pug
├── tsconfig.json
└── web
```

## Estructura y detalles

Es un proyecto desarrollado por capas, agrupadas en diferentes folders:

```bash
.
├── documentation
├── logs
├── src
│   ├── config (1)
│   ├── controllers (2)
│   │   └── modules
│   ├── dal (3)
│   │   ├── db (4)
│   │   ├── models (5)
│   │   │   └── mongo
│   │   └── repositories (6)
│   │       └── mongo
│   │           └── modules
│   ├── lib (7)
│   │   └── auth (8)
│   ├── middlewares (9)
│   ├── routes (10)
│   │   └── modules
│   ├── server (11)
│   ├── services (12)
│   │   └── modules
│   ├── types (13)
│   │   ├── modules
│   │   └── schemas (14)
│   └── views (15)
└── web
    ├── public
    └── src
        ├── components
        │   └── Auth
        ├── store
        └── utils
```

1. `config`: contiene 2 archivos
   - `globals.ts` que levanta la configuración del .env y el process.argv
   - `nodemailer.config.ts` con la configuración para 2 posibles configuraciones para envío de mail: Gmail y Ethereal.
2. `controllers`contienen la lógica de cada ruta, invocando los servicios y libs que requieran
3. `dal` carpeta general con todo lo referido al acceso a datos: modelo, conexión y repositorios
4. `db` objeto para instanciar una nueva base de datos
5. `models` Modelos de la persistencia para cada entidad del crud
6. `repositories` Repositorios para la persistencia de las entidades del crud: También es posible intercambiar repos, modelo desde el factory method que se encuentra en [index.ts](https://gitlab.com/murreli1981/coder-proyecto-final/-/blob/main/src/dal/repositories/index.ts)
7. `libs` algunos componentes adicionales para soporte del proyecto:
   - `email.utils.ts` template para un body de respuesta por email al realizar una orden
   - `jwt.utils.ts` métodos para la generación, verificación de un jsonwebtoken, también válida qeu el password encriptado sea el correcto
   - `logger.ts` configuración del logger
   - `notification.utils.ts` clase que envía por email las notificaciones de negocio
8. `auth`
   - `passport-jwt.utils` configuración de la estrategia de passport-jwt
9. `middlewares`: middlewares de express
   - `authentication.middleware.ts` llama a la estrategia de passport-jwt para validar que el token sea válido
   - `admin.middleware.ts` valida si el usuario tiene el rol de admin
10. `routes` definición de las rutas del crud
11. `contiene el server de express y el websocket
12. `services` implementación de la capa de servicios: Por un lado se encuentran los servicios del crud y por otro un `extras.service.ts`que obtiene la configuracion del servidor y `notificacion service` que llama a la implementación de nodemailer para el envío de notificaciones
13. `types` contiene las interfaces para los objetos del ecommerce
14. `schemas` definición de los esquemas utilizando joi para validaciones en los servicios
15. `views` vistas del servidor con ejs y pugs: `chat-admin.ejs` vista para el chat de un administrador y `config.pug`vista con detalles de la configuracion y file system

## Autenticación

Se realiza con la librería [passport](passport) y utilizando [jwt](jwt.io) -.
Es posible setear el tiempo de expiración desde el [.env](https://gitlab.com/murreli1981/coder-proyecto-final/-/blob/main/.env.example), en formato segundos:

`JWT_EXPIRATION=3600`

Ej:

- 60 = 1 minuto
- 3600 = 1 hora
- 86400 = 1 día

```
 (CLIENT)                        (SERVER)
    ||  (user/login user/register)  ||
    ||----------------------------->||
    ||                              ||
    ||  (user/login user/register)  ||
    ||<-----------------------------||
    ||       (JWT + user data)      ||
    ||                              ||
    ||                              ||
    ||                              ||
    ||            CRUD              ||
    ||   (/user /messages /order    ||
    ||        /shoppingcart)        ||
    ||           + JWT              ||
    ||----------------------------->||
    ||                              ||
    ||   (/user /messages /order    ||
    ||        /shoppingcart)        ||
    ||<-----------------------------||
    ||            (data)            ||
    ||                              ||
```

### endpoints authenticacion:

Es posible obtener el token de autenticación invocando cualquiera de los siguientes endpoints:

- server:port/user/register
- server:port/user/login

Ejemplo de respuesta

```json
{
  "user": {
    "_id": "61ce4e2ad6919f3e6b70361f",
    "username": "mohammed_nader18@hotmail.com",
    "password": "e5dbe162e1b5fd35143f55bd8fe8706e87cb4e659dbf5b274385408a52797fb6796dac723559f75dcbd583dea95d3c83a799247a57d7633091e708d8fa499b3e",
    "name": "Miss Sandra Boyle",
    "age": 40,
    "address": "fitte 1755",
    "phone": "1137878904",
    "admin": false,
    "salt": "9aa6e53749c8070ae7f9f6b79c31248bbac528d5a75e7a0bdf2b45587ffdea1f",
    "id": "61ce4e2ad6919f3e6b70361f"
  },
  "token": "Bearer akldjalkdjakjaskjajaksasjaskdjask"
}
```

Luego incluir el token de la siguiente forma (ej: curl)

```bash
curl --location --request GET 'localhost:8000/product' \
--header 'Authorization: Bearer akldjalkdjakjaskjajaksasjaskdjask'
```

### Endpoints:

la api rest contiene un crud para las siguientes entidades

- user
- product
- shoppingcart
- order

adicional al crud se encuentran 2 endpoints

- whoami (Valida el token)
- prefix (Devuelve el listado de prefijos internacionales para un numero telefonico)

El listado completo de endpoints está disponible en la colección de postman

[`documentation/CH-BE-13440-entrega-final.postman_collection.json`](https://gitlab.com/murreli1981/coder-proyecto-final/-/blob/main/documentation/CH-BE-13440-entrega-final.postman_collection.json)

[`Documentación online`](https://documenter.getpostman.com/view/496254/UVRHjink)

## Iniciando el proyecto:

### .env

luego de clonar el repo es necesario completar la configuración del servidor, la misma se realiza desde el .env.

```bash
SERVER_PORT=8000
#SERVER_CLUSTER [true,(vacio)]
SERVER_CLUSTER=
#LOGGER [default, debug]
LOGGER_MODE=debug
LOG_FILE_PATH=./logs/error.log

#NODEMAILER [GMAIL, ETHEREAL]
ADMIN_MAILBOX=adminemail
EMAIL_SERVER=GMAIL
#EMAIL_ACCOUNT [coder.murreli@gmail.com, isai.quigley2@ethereal.email]
EMAIL_ACCOUNT=email
#EMAIL_PASSWORD [isxbpmgheigktzaf, FGTAd3H16MWPVTDQ5K]
EMAIL_PASSWORD=password

#SERVER'S HOST
HOSTNAME_PROD=https://be-13440-murreli.herokuapp.com
HOSTMANE_DEV=http://localhost

#DB [mongo, ...]
DB=mongo

#CONNECTION_STRING
MONGO_PROD=mongodb+srv://[user]:[password]@cluster0.k0hps.mongodb.net/ecommerce?retryWrites=true&w=majority
MONGO_DEV=mongodb://localhost:27017/ecommerce?retryWrites=true&w=majority

#JWT_EXPIRATION (seconds)
JWT_EXPIRATION=3600
```

también se encuentra en el repo para renombrarlo.

[.env.example](https://gitlab.com/murreli1981/coder-proyecto-final/-/blob/main/.env.example)

### levantar el servidor

Es posible levantar el servidor con 2 configuraciones

- `npm run start:prod`: Inicia el servidor en modo producción (mongo atlas)
- `npm run start:dev`: Inicia el servidor en modo desarrollo (mongo local)

### Web de pruebas

Adjunta en el repo hay una web de prueba dentro de la carpeta
[web](https://gitlab.com/murreli1981/coder-proyecto-final/-/tree/main/web) en el root del proyecto

para iniciarla:

1. ejecutar `cd web`
2. correr `npm i`
3. ir a [config.js](https://gitlab.com/murreli1981/coder-proyecto-final/-/blob/main/web/src/utils/config.js) y cambiar el host por el utilizado: por default localhost:8000
4. correr `npm start`

### flujos

1. se inicia el front en localhost:3000. el front verifica a través del endpoint `user/whoami` que el token almacenado en el session.storage sea válido:
   - Si no hay jwt o está vencido hace un render del login
   - Si el token es válido dibuja la home del ecommerce, mientras recupera los datos del usuario al que pertenece el token.
2. Admin: true/false
   - Si el usuario tiene un perfil de administrador mostrará la posibilidad de agregar, editar o borrar productos
   - Si el usuario no es administrador accederá al mismo home pero sin la posibilidad de agregar o editar eliminar productos
   - Todos los usuarios se crean con el flag de admin en false: para cambiarlo se puede usar el PATCH de `users/:id` con el body `{"admin": true}` (ver [`documentation/CH-BE-13440-entrega-final.postman_collection.json`](https://gitlab.com/murreli1981/coder-proyecto-final/-/blob/main/documentation/CH-BE-13440-entrega-final.postman_collection.json))
3. Listado de productos. En el listado izquierdo se encuentra una vista de tarjetas que listan cada producto cargado en el sistema. También es posible utilizar alguno de los filtros implementados en la vista para buscar un producto específico o rango:
   - filtrar por nombre exacto
   - por código exacto
   - por precio mínimo
   - por precio máximo
   - por rango de precio
   - por stock mínimo
   - por stock máximo
   - por rango de stock
4. Carrito: el registrarse un usuario este no posee un carrito, el front mandará a crearlo la primera vez que se intente agregar un producto, al existir un carrito y al menos 1 item en él se habilita la opción de enviar orden
5. Checkout: el botón de enviar orden llama al endpoint de checkout y este:
   - crea una orden en la base
   - elimina de la base el carrito
   - envía una notificación por email al mail administrador definido en el `.env`, `ADMIN_MAILBOX` con el resumen de la orden
6. Chat:
   - Es un chat privado entre 1 usuario y admin
   - ningun usuario puede ver las conversaciones del otro ya que cada socket hace un join a una sala única.
   - Uno o varios admin pueden acceder a todas las salas privadas que quieran, basta conocer el mail de la persona que quiere contactar
   - El usuario de mail a@a.com puede acceder al chat del administrador desde el botón 'CHATEAR CON SOPORTE' ubicado en la parte inferior derecha de la vista.
   - Al hacer click se dispara un correo al mail administrador definido en el `.env`, `ADMIN_MAILBOX` en el correo se encuentra un link al chat
   - Ej: http://localhost:8000/chat-admin?room=a@a.com, esta es una vista del server: (chat-admin.ejs) y el administrador, desde esa url se une a la sala "a@a.com" indicada en el query param.
   - si el usuario b@b.com ingresa a CHATEAR CON SOPORTE, se repiten los pasos anteriores pero nunca accede a la conversación del usuario anterior
   - Todas las conversaciones se guardan en la base, luego se recuperan haciendo un get a /message y usando el filtro de room=mail-del-usuario
   - Si un usuario o admin ingresa o sale al chat la otra parte recibirá una notificación
   - El admin true/false del user no está relacionado con el admin del chat
7. Informacion del sistema, `http://localhost:8000/system` muestra inforamción del servidor, como memoria, procesador, y parámetros, la vista está realizada en pug `config.pug`

### Heroku

host: https://be-13440-murreli.herokuapp.com

El servidor tiene la configuración de producción, persiste los datos en Mongo atlas.
Se puede apuntar la web de prueba desde su [config.js](https://gitlab.com/murreli1981/coder-proyecto-final/-/blob/main/web/src/utils/config.js)
Aunque no es muy recomendable, :P

### Pendientes:

- Control de stock: es un dato pero que no tiene lógica de negocio, si bien al agregar un producto al carrito podría hacer un patch al producto agregado restando el stock, o sumar cuando se elimina del carrito, tampoco hay una validación que no permita agregar un producto si no tiene mas stock.

- Vista de errores: el ejercicio indicaba que había que armar una vista con ejs donde figure el error y el código. No la implementé pero sí el back devuelve el código y un mensaje en el caso de que haya una validación de negocio (ejemplo postear algo que no coincida en la validación del schema devuelve un 400 con un msg del error)

- Bugs en el front: Hay muchos, no maneja muy bien los estados.
  - Al agregar e inmediatamente editar quedan algunos datos precargados, se soluciona refrescando la página
  - empty states: si el back no está operativo el sitio crashea
  - visuales, usarlo en una resolución alta, sinó las tarjetas se rompen
  - useEffect, recarga todo varias veces, ejemplo si estás en el chat y querés agregar un producto empieza una recarga infinita y crashea
  - error handling, no tiene, no lee los errores que llegan del back
